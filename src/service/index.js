import axios from 'axios'

const $http = axios;
// $http.defaults.baseURL = "http://3.17.174.41/";
$http.defaults.headers["Content-Type"] = "application/json;charset=UTF-8";
$http.defaults.headers.common["Accept"] = "application/json";
const token = '949493360:AAFxCnB4Cky_-LEkGMu1gbzEl2zc8zOcL34';
const chat_id = '-426794697';
export default {
  send_message_to_telegram(data) {
    return $http.post(`https://api.telegram.org/bot${token}/sendMessage?chat_id=${chat_id}&parse_mode=html&text=${data}`)
  }
}