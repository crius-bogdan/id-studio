import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pageName: 'Agency',
    navigationFullColor: false,
    window_width: window.innerWidth
  },
  getters: {
    window_width: state => {
      return state.window_width > 1190
    }
  },
  mutations: {
    CHANGE_PAGE_NAME(state, name) {
      state.pageName = name;
    },
    SET_FULL_COLOR_NAVIGATION(state, bool) {
      state.navigationFullColor =  bool
    },
    SET_WINDOW_WIDTH(state, width) {
      state.window_width = width
    }
  },
  actions: {
    change_page_name({ commit }, name) {
      commit('CHANGE_PAGE_NAME', name)
    },
    set_full_color_navigation({ commit }, bool) {
      commit('SET_FULL_COLOR_NAVIGATION', bool)
    },
    resize_document({ commit }, width) {
      commit('SET_WINDOW_WIDTH', width)
    }
  },
  modules: {}
})
